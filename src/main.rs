#[macro_use] extern crate gfx;
extern crate gfx_device_gl;
extern crate gfx_window_glutin;
extern crate glutin;

const VERTICES: [Vertex; 4] = [
    Vertex { position: [ 1.,  1.] },
    Vertex { position: [-1.,  1.] },
    Vertex { position: [-1., -1.] },
    Vertex { position: [ 1., -1.] },
];

const INDICES: [u8; 6] = [0, 1, 2, 2, 3, 0];

gfx_vertex_struct! { Vertex {
    position: [f32; 2] = "a_Position",
}}

gfx_pipeline! { pipe {
    vertex: gfx::VertexBuffer<Vertex> = (),
    translate: gfx::Global<[f32; 2]> = "u_Translate",
    scale: gfx::Global<[f32; 2]> = "u_Scale",
    iterations: gfx::Global<i32> = "u_Iterations",
    out: gfx::RenderTarget<gfx::format::Srgb8> = "o_Color",
}}

fn main() {
    use gfx::traits::{ Device, Factory, FactoryExt };
    
    let args: Vec<String> = std::env::args().collect();
    let w = args[1].parse().unwrap();
    let h = args[2].parse().unwrap();
    let zoom: f32 = args[3].parse().unwrap();
    let translate_re = args[4].parse().unwrap();
    let translate_im = args[5].parse().unwrap();
    let iterations = args[6].parse().unwrap();

    let builder = glutin::WindowBuilder::new()
        .with_dimensions(w, h)
        .with_title("Fracture".to_string());
    let (window, mut device, mut factory, main_color, _) =
        gfx_window_glutin::init::<gfx::format::Srgb8, gfx::format::Depth>(builder);
    let mut encoder = factory.create_encoder();

    let pso = factory.create_pipeline_simple(
        include_bytes!("../shaders/main.glslv"),
        include_bytes!("../shaders/mandelbrot.glslf"),
        gfx::state::CullFace::Nothing,
        pipe::new()
    ).unwrap();

    let vertices = factory.create_buffer_static(&VERTICES, gfx::BufferRole::Vertex);
    let indices = factory.create_buffer_static(&INDICES, gfx::BufferRole::Index);

    let data = pipe::Data {
        vertex: vertices,
        translate: [translate_re, translate_im],
        scale: [1./zoom, 1./zoom],
        iterations: iterations,
        out: main_color
    };

    let mut slice: gfx::Slice<_> = indices.into();

    'l: loop {
        for event in window.poll_events() {
            match event {
                glutin::Event::Closed => break 'l,
                _ => {}
            }
        }

        encoder.reset();
        encoder.clear(&data.out, [0.0, 0.0, 0.0]);
        encoder.draw(&slice, &pso, &data);

        device.submit(encoder.as_buffer());
        window.swap_buffers().unwrap();
        device.cleanup();
    }
}
