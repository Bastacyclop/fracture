#version 150 core

in vec2 v_ComputePosition;

out vec4 o_Color;

uniform int u_Iterations;

vec2 complex_square(vec2 z) {
    return vec2(z.x*z.x - z.y*z.y, 2*z.x*z.y);
}

void main() {
    uint max_i = uint(u_Iterations);

    vec2 z = vec2(0.0, 0.0);
    vec2 c = v_ComputePosition;

    uint i = 0u;
    while (i < max_i) {
        z = complex_square(z) + c;

        if ((z.x*z.x + z.y*z.y) > 2.0) break;
        i += 1u;
    }

    if (i == max_i) {
        o_Color = vec4(1.0, 1.0, 1.0, 1.0);
    } else {
        o_Color = vec4(0.0, 0.0, 0.0, 1.0);
    }
}
