#version 150 core

in vec2 a_Position;

out vec2 v_ComputePosition;

uniform vec2 u_Translate;
uniform vec2 u_Scale;

void main() {
    gl_Position = vec4(a_Position, 0.0, 1.0);

    v_ComputePosition = (a_Position*u_Scale) + u_Translate;
}
